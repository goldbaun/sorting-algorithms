#ifndef SORTING_ALGORITHMS_INTROSORT_H
#define SORTING_ALGORITHMS_INTROSORT_H
#include <vector>
#include <algorithm>
#include <cmath>
#include <future>
#include <thread>
// sortowanie introspektywne

#define THRESHOLD 4000  // based on testing
#define MINIMAL_PARTITION_SIZE 16 // based on information I found on internet

template <typename T>
class IntroSort
{
private:
    typename std::vector<T>::iterator partition(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right);
    void introSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, long depthSize);
    void aintroSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, long depthSize);
    void insertionSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
    typename std::vector<T>::iterator choosePivot(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right, typename std::vector<T>::iterator middle);

public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
    void asort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
typename std::vector<T>::iterator IntroSort<T>::choosePivot(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right, typename std::vector<T>::iterator middle)
{
    T maxValue = std::max(*left, *right);
    maxValue = std::max(maxValue, *middle);
    if (maxValue == *left)
        return left;
    if (maxValue == *right)
        return right;
    else
        return middle;
}

template <typename T>
void IntroSort<T>::insertionSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)       
{
    for (auto i = start; i != end; std::advance(i, 1))
        std::rotate(std::upper_bound(start, i, *i), i, std::next(i));
}

template <typename T>
typename std::vector<T>::iterator IntroSort<T>::partition(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right)
{
    auto pivot = std::prev(right, 1);
    auto smallerValue = left;
    for (auto biggerValue = left; biggerValue != pivot; std::advance(biggerValue, 1))
    {
        if (*biggerValue <= *pivot)
        {
            std::swap(*biggerValue, *smallerValue);
            std::advance(smallerValue, 1);
        }
    }
    std::swap(*smallerValue, *pivot);
    return smallerValue;
}

template <typename T>
void IntroSort<T>::introSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, long depthSize)
{
    if (std::distance(start, end) < MINIMAL_PARTITION_SIZE)
    {
        insertionSort(start, end);
        return;
    }

    if (depthSize == 0)
    {
        std::make_heap(start, end); // using stl implementation of heapsort
        std::sort_heap(start, end);
        return;
    }

    auto pivot = choosePivot(start, end, std::next(start, (std::distance(start, end) / 2)));
    std::swap(*pivot, *end);

    auto partitionPoint = partition(start, end);
    introSort(start, partitionPoint, depthSize - 1);
    introSort(partitionPoint, end, depthSize - 1);
}

template <typename T>
void IntroSort<T>::aintroSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, long depthSize)       // async introsort
{
    if (std::distance(start, end) < MINIMAL_PARTITION_SIZE)
    {
        insertionSort(start, end);
        return;
    }

    if (depthSize == 0)
    {
        std::make_heap(start, end); // using stl implementation of heap
        std::sort_heap(start, end);
        return;
    }

    auto pivot = choosePivot(start, end, std::next(start, (std::distance(start, end) / 2)));
    std::swap(*pivot, *end);

    if (std::distance(start, end) >= THRESHOLD)      
    {
        auto partitionPoint = partition(start, end);
        auto fut1 = std::async([&]() {
            introSort(start, partitionPoint, depthSize - 1);
        });
        introSort(partitionPoint, end, depthSize - 1);
        fut1.wait();
    }
    else
    {
        auto partitionPoint = partition(start, end);
        introSort(start, partitionPoint, depthSize - 1);
        introSort(partitionPoint, end, depthSize - 1);
    }
}

template <typename T>
void IntroSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
{
    long maxDepth = 2 * log(std::distance(start, end));
    introSort(start, end, maxDepth);
}

template <typename T>
void IntroSort<T>::asort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)        // async introsort
{
    long maxDepth = 2 * log(std::distance(start, end));
    aintroSort(start, end, maxDepth);
}
#endif //SORTING_ALGORITHMS_INTROSORT_H
