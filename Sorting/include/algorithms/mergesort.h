#ifndef SORTING_ALGORITHMS_MERGESORT_H
#define SORTING_ALGORITHMS_MERGESORT_H
#include <vector>
#include <algorithm>
#include <utility>

// sortowanie przez scalanie

template <typename T>
class MergeSort
{
private:
    void merge(typename std::vector<T>::iterator start, typename std::vector<T>::iterator middle, typename std::vector<T>::iterator end);

public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
void MergeSort<T>::merge(typename std::vector<T>::iterator start, typename std::vector<T>::iterator middle, typename std::vector<T>::iterator end)
{
    std::vector<T> tmp;
    tmp.reserve(std::distance(start, end));
    auto left = start;
    auto right = middle;
    while (left != middle && right != end)
    {
        if (*right < *left)
        {
            tmp.emplace_back(*right);
            std::advance(right, 1);
        }
        else
        {
            tmp.emplace_back(*left);
            std::advance(left, 1);
        }
    }
    tmp.insert(tmp.end(), left, middle);
    tmp.insert(tmp.end(), right, end);

    std::move(tmp.begin(), tmp.end(), start);
}

template <typename T>
void MergeSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
{
    if (std::distance(start, end) > 1)
    {
        auto middle = std::next(start, (std::distance(start, end) / 2));
        sort(start, middle);
        sort(middle, end);
        merge(start, middle, end);
    }
}
#endif //SORTING_ALGORITHMS_MERGESORT_H
