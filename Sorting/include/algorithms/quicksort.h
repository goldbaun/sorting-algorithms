#ifndef SORTING_ALGORITHMS_QUICKSORT_H
#define SORTING_ALGORITHMS_QUICKSORT_H
#include <vector>
#include <algorithm>
#include <future>
#include <thread>
//quicksort

#define THRESHOLD 4000 // based on testing

template <typename T>
class QuickSort
{
private:
    typename std::vector<T>::iterator partition(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right);

public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
    void asort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end); // async sort
};



template <typename T>
typename std::vector<T>::iterator QuickSort<T>::partition(typename std::vector<T>::iterator left, typename std::vector<T>::iterator right)
{
    auto pivot = std::prev(right, 1);
    auto smallerValue = left;
    for (auto biggerValue = left; biggerValue != pivot; std::advance(biggerValue, 1))
    {
        if (*biggerValue <= *pivot)
        {
            std::swap(*biggerValue, *smallerValue);
            std::advance(smallerValue, 1);
        }
    }
    std::swap(*smallerValue, *pivot);
    return smallerValue; // return new pivot
}

template <typename T>
void QuickSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
{
    if (std::distance(start, end) > 1)
    {
        auto pivot = partition(start, end);
        sort(start, pivot);
        sort(pivot, end);
    }
}

template <typename T>
void QuickSort<T>::asort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // async quicksort
{
    if (std::distance(start, end) > 1)
    {
        auto pivot = partition(start, end);

        if (std::distance(start, end) >= THRESHOLD)
        {
            auto fut1 = std::async([&]() {
                sort(start, pivot);
            });
            sort(pivot, end);
            fut1.wait();
        }
        else
        {
            sort(start, pivot);
            sort(pivot, end);
        }
    }
}
#endif //SORTING_ALGORITHMS_QUICKSORT_H
