// Still working on testing algorithms 

#include <iostream>
#include <random>
#include <chrono>
#include <fstream>
#include "algorithms/quicksort.h"
#include "algorithms/mergesort.h"
#include "algorithms/introsort.h"

#define VECTOR_SIZE 100'000  // size of testing vector
#define LOOPS 100     // number of sorting loops

int main(int argc, char *argv[])
{
    char c;
    QuickSort<long> quickSort;
    MergeSort<long> mergeSort;
    IntroSort<long> introSort;
    std::vector<long> testVector;
    std::random_device rd;      // random numbers generator
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, 1000000);
    std::uniform_int_distribution<> dis1(0.25 * VECTOR_SIZE, 1000000);
    std::uniform_int_distribution<> dis2(0.5 * VECTOR_SIZE, 1000000);
    std::uniform_int_distribution<> dis3(0.75 * VECTOR_SIZE, 1000000);
    std::uniform_int_distribution<> dis4(0.95 * VECTOR_SIZE, 1000000);
    std::uniform_int_distribution<> dis5(0.99 * VECTOR_SIZE, 1000000);
    std::uniform_int_distribution<> dis6(0.997 * VECTOR_SIZE, 1000000);
    std::ofstream outdata;      // output streams 
    std::ofstream outdatare;
    std::ofstream outdata25;
    std::ofstream outdata50;
    std::ofstream outdata75;
    std::ofstream outdata95;
    std::ofstream outdata99;
    std::ofstream outdata997;

    outdatare.open("sort.txt");     // random elements
    for (int i = 0; i < LOOPS; i++)
    {

        for (int i = 0; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdatare << duration.count() << std::endl;
        testVector.erase(testVector.begin(), testVector.end());
    }
    outdatare.close();

    outdatare.open("sortreverse.txt");      // elements sorted in reverse order
    for (int i = 0; i < LOOPS; i++)
    {

        for (int i = 0; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis(gen));
        }
        std::sort(testVector.begin(), testVector.end(), std::greater<long>());
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdatare << duration.count() << std::endl;
        testVector.erase(testVector.begin(), testVector.end());
    }
    outdatare.close();

    outdata25.open("sort25.txt");       // first 25% of elements sorted 
    long newSize = 0.25*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis1(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata25 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata25.close();

    outdata50.open("sort50.txt");       // first 50% of elements sorted 
    newSize = 0.5*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis2(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata50 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata50.close();

    outdata75.open("sort75.txt");       // first 75% of elements sorted 
    newSize = 0.75*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis3(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata75 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata75.close();

    outdata95.open("sort95.txt");       // first 95% of elements sorted 
    newSize = 0.95*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis4(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata95 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata95.close();

    outdata99.open("sort99.txt");       // first 99% of elements sorted 
    newSize = 0.99*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis5(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata99 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata99.close();

    outdata997.open("sort997.txt");     // first 99.7% of elements sorted 
    newSize = 0.997*VECTOR_SIZE;
    for (int i = 0; i < LOOPS; i++)
    {
        for (long i = 0; i <= newSize; i++)
        {
            testVector.push_back(i);
        }

        for (long i = newSize; i < VECTOR_SIZE; i++)
        {
            testVector.push_back(dis6(gen));
        }
        auto start = std::chrono::high_resolution_clock::now();
        quickSort.asort(testVector.begin(), testVector.end());
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "Time in microseconds: " << duration.count() << std::endl;
        outdata997 << duration.count() << std::endl;
        testVector.erase(testVector.begin(),testVector.end());
    }
    outdata997.close();

    std::cout << "End" << std::endl;
    std::cin >> c;
    return 0;
}
